
function [E] = compute_score(sz, mask_sz, mask, pts, cnts)

E = zeros(sz);
for i = 1 : sz(1)
    % fprintf('.');
    for j = 1 : sz(2)
        E(i, j) = mask(pts)*cnts' ; % [0 - 1], lower the better
        pts = pts + 1;    
    end
    pts = pts + mask_sz(1) - sz(2);    
end