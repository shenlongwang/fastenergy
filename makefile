#
# Change to your own MATLABROOT
MATLABROOT:=/pkgs/matlab-R2015b
#

#
# Defaults
#

MEX=$(MATLABROOT)/bin/mex

# The following are the definitions for each target individually.

mexGPUExample:   compute_score_gpu.cu
	$(MEX) -v -f glnxa64/mexopts.sh $^
