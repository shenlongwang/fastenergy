/*
 * Example of how to use the mxGPUArray API in a MEX file.  This example shows
 * how to write a MEX function that takes a gpuArray input and returns a
 * gpuArray output, e.g. B=mexFunction(A).
 *
 * Copyright 2012 The MathWorks, Inc.
 */

#include "mex.h"
#include "gpu/mxGPUArray.h"

/*
 * Device code
 */

#define SZ 401
#define N 401*401
#define MASK_SZ 2001

void __global__ ComputeScore(int const * const mask,
                         int const * const pts,
                         int const * const cnts,
                         int * const E,
                         int const N_pts)
{
    /* Calculate the global linear index, assuming a 1-d grid. */
    int const p = blockDim.x * blockIdx.x + threadIdx.x;
    if (p < N) {
        int j = p / SZ;
        int i = p % SZ;
        int score = 0;
        for (int k = 0; k < N_pts; k++)
            score += (mask[pts[k] + i*MASK_SZ+j] * cnts[k]); // [0 - 1], lower the better
		E[p] = score;
		// E[p] = 0;
    }
}

// void computeScore(int* sz, int* mask_sz, int* mask, int* pts, int* cnts, int* E, int N_pts)
// {
// 	int i, j, k, p;
// 	int offset = sz[0] / 2 + 1;
// 	int n_proposal = sz[0] * sz[1];
//     for (p = 0; p < n_proposal; ++p)
//     {
// 		j = p / sz[1];
// 		i = p % sz[1];
//         int score = 0;
// #pragma omp parallel for private(k) reduction(+:score)
//         for (k = 0; k < N_pts; k++)
//             score += mask[pts[k] + i * mask_sz[0] + j]*cnts[k]; // [0 - 1], lower the better
// 		E[p] = score;
//     }
// }


/*
 * Host code
 */
void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, mxArray const *prhs[])
{
    /* Declare all variables.*/
    // int* sz = (int *)mxGetData(prhs[0]);
    // int* mask_sz = (int *)mxGetData(prhs[1]);
    
    mxGPUArray const *sz;
    mxGPUArray const *mask_sz;
    mxGPUArray const *mask;
    mxGPUArray const *pts;
    mxGPUArray const *cnts;
    mxGPUArray const *E_0;
    mxGPUArray *E;
    
    char const * const errId = "parallel:gpu:mexGPUExample:InvalidInput";
    char const * const errMsg0 = "Not GPU Array";
    char const * const errMsg1 = "Before";
    char const * const errMsg2 = "After";
    char const * const errMsg = "Invalid input to MEX file.";

    /* Choose a reasonably sized number of threads for the block. */
    int const threadsPerBlock = 256;
    int blocksPerGrid;

    /* Initialize the MathWorks GPU API. */
    mxInitGPU();

    /* Throw an error if the input is not a GPU array. */
    if ((nrhs!=6) ||!(mxIsGPUArray(prhs[0])) || !(mxIsGPUArray(prhs[1])) || !(mxIsGPUArray(prhs[2])) || !(mxIsGPUArray(prhs[3])) || !(mxIsGPUArray(prhs[4]))) {
        mexErrMsgIdAndTxt(errId, errMsg0);
    }

    sz = mxGPUCreateFromMxArray(prhs[0]);
    mask_sz = mxGPUCreateFromMxArray(prhs[1]);
    mask = mxGPUCreateFromMxArray(prhs[2]);
    pts = mxGPUCreateFromMxArray(prhs[3]);
    cnts = mxGPUCreateFromMxArray(prhs[4]);
    E_0 = mxGPUCreateFromMxArray(prhs[5]);
    
    int N_pts = (int)(mxGPUGetNumberOfElements(pts));
    int N_cts = (int)(mxGPUGetNumberOfElements(cnts));
    int const * d_sz = (int const *)(mxGPUGetDataReadOnly(sz));
    // mexPrintf("%s, %d", d_sz[0]);
    // mexPrintf("%s, %d", d_sz[1]);
    // int N = d_sz[0] * d_sz[1];
    /*
     * Verify that A really is a int32 array before extracting the pointer.
     */
    if ((mxGPUGetClassID(mask) != mxINT32_CLASS) || (N_pts != N_cts)) {
        mexErrMsgIdAndTxt(errId, errMsg);
    }

    /*
     * Now that we have verified the data type, extract a pointer to the input
     * data on the device.
     */
    int const * d_mask_sz = (int const *)(mxGPUGetDataReadOnly(mask_sz));
    int const * d_mask = (int const *)(mxGPUGetDataReadOnly(mask));
    int const * d_pts = (int const *)(mxGPUGetDataReadOnly(pts));
    int const * d_cnts = (int const *)(mxGPUGetDataReadOnly(cnts));
    
    /* Create a GPUArray to hold the result and get its underlying pointer. */
    // E = mxGPUCreateGPUArray(ndims, dims, mxINT32_CLASS, mxREAL, MX_GPU_DO_NOT_INITIALIZE);
    E = mxGPUCreateGPUArray(mxGPUGetNumberOfDimensions(E_0),
                            mxGPUGetDimensions(E_0),
                            mxGPUGetClassID(E_0),
                            mxGPUGetComplexity(E_0),
                            MX_GPU_DO_NOT_INITIALIZE);
    int* d_E = (int *)(mxGPUGetData(E));

    /*
     * Call the kernel using the CUDA runtime API. We are using a 1-d grid here,
     * and it would be possible for the number of elements to be too large for
     * the grid. For this example we are not guarding against this possibility.
     */
    blocksPerGrid = (N + threadsPerBlock - 1) / threadsPerBlock;
    ComputeScore<<<blocksPerGrid, threadsPerBlock>>>(d_mask, d_pts, d_cnts, d_E, N_pts);

    /* Wrap the result up as a MATLAB gpuArray for return. */
    plhs[0] = mxGPUCreateMxArrayOnGPU(E);

    /*
     * The mxGPUArray pointers are host-side structures that refer to device
     * data. These must be destroyed before leaving the MEX function.
     */
    mxGPUDestroyGPUArray(sz);
    mxGPUDestroyGPUArray(mask_sz);
    mxGPUDestroyGPUArray(mask);
    mxGPUDestroyGPUArray(pts);
    mxGPUDestroyGPUArray(cnts);
    mxGPUDestroyGPUArray(E);
}
