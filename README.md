# CUDA based Fast Score Evaluation 

This repo is the folder for Energy Evaluation Code.

This tool could help efficiently compute scores from millions of query points over a volume:
```
#!latex
$\sum_i E(x_i, y_i, z_i)$, where $E$ is a pre-computed volume and $x_i, y_i, z_i$ is the index. 

```
This function can be used in many tasks, such as point cloud matching, camera localization, etc. 

### Dependencies
- [CUDA](https://developer.nvidia.com/cuda-toolkit)

### Compiling 
- Make sure the path `mex.h` is included in your environment variable
- Run `make all`

### Running
- Please run `test_compute_score.m` for test.