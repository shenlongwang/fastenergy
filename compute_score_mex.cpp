
#include "mex.h"
#include <omp.h>
//#include <iostream>
//using namespace std;

// sz, mask_sz, mask, pts, cnts

void computeScore(int* sz, int* mask_sz, int* mask, int* pts, int* cnts, int* E, int N_pts)
{
	int i, j, k, p;
	int offset = sz[0] / 2 + 1;
	int n_proposal = sz[0] * sz[1];
    for (p = 0; p < n_proposal; ++p)
    {
		j = p / sz[1];
		i = p % sz[1];
        int score = 0;
#pragma omp parallel for private(k) reduction(+:score)
        for (k = 0; k < N_pts; k++)
            score += mask[pts[k] + i * mask_sz[0] + j]*cnts[k]; // [0 - 1], lower the better
		E[p] = score;
    }
}

void mexFunction( int nl, mxArray *pl[], int nr, const mxArray *pr[] )
{
    if (nr != 5 || nl != 1)
    {
        mexErrMsgTxt("Check the input.\nUsage: E = compute_score(sz, mask_sz, mask, pts, cnts)\n");
    }
    
    // boxes is a 6xN matrix
    int* sz = (int *)mxGetData(pr[0]);
    int* mask_sz = (int *)mxGetData(pr[1]);
    int M_mask = (int)mxGetM(pr[2]);
    int N_mask = (int)mxGetN(pr[2]);
    int* mask = (int *)mxGetData(pr[2]);
    int* pts = (int *)mxGetData(pr[3]);
    int* cnts = (int *)mxGetData(pr[4]);
    
    int N_pts = (int)mxGetN(pr[3]);
    int N_cts = (int)mxGetN(pr[4]);
    
    if (M_mask  != mask_sz[0])
        mexErrMsgTxt("Size of the mask matrix should be equal to mask_sz! \n");
    if (N_pts  != N_cts)
        mexErrMsgTxt("Size of pts and cts should be equal! \n");
        
    // Output
    pl[0] = mxCreateNumericMatrix(sz[0], sz[1], mxINT32_CLASS, mxREAL);
    int* E = (int*) mxGetData(pl[0]);
    computeScore(sz, mask_sz, mask, pts, cnts, E, N_pts);
}
