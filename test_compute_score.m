% clc;clear;close all;
% if ispc
%     mex OPTIMFLAGS="/Ox /openmp" compute_score_mex.cpp;
% else
%     mex COMPFLAGS='$COMPFLAGS -DNDEBUG -openmp -O3' compute_score_mex.cpp;
% end
load temp_debug;

sz = int32([length(x_range), length(y_range)]);
mask_sz = int32(im_size);
mask = int32(building_contour);
pts = int32(val_building);
cnts = int32(count_building);
E = int32(zeros(sz));

sz_gpu = gpuArray(sz);
mask_sz_gpu = gpuArray(mask_sz);
cnts_gpu = gpuArray(cnts);
pts_gpu = gpuArray(pts);
mask_gpu = gpuArray(mask);
E = gpuArray(E);
tic; [E_building_gpu] = compute_score_gpu(sz_gpu, mask_sz_gpu, mask_gpu, pts_gpu, cnts_gpu, E);
fprintf('GPU: %2.6f\n', toc);
tic; [E_building_mex] = 1 - double(compute_score_mex(sz, mask_sz, mask, pts, cnts)) / 20000;
fprintf('MEX: %2.6f\n', toc);
tic; [E_building] = 1 - double(compute_score(sz, mask_sz, double(mask), pts, double(cnts))) / 20000;
fprintf('MATLAB: %2.6f\n', toc);

E_building_gpu = 1 - double(gather(E_building_gpu)) / 20000;

figure;imagesc([E_building_gpu, E_building_mex, E_building]);

sz = int32([length(x_range), length(y_range)]);
mask_sz = int32(im_size);
mask = int32(building_contour);
pts = int32(val_freespace);
cnts = int32(count_freespace);
sz_gpu = gpuArray(sz);
mask_sz_gpu = gpuArray(mask_sz);
cnts_gpu = gpuArray(cnts);
pts_gpu = gpuArray(pts);
mask_gpu = gpuArray(mask);
E = gpuArray(E);
tic; [E_freespace_gpu] = compute_score_gpu(sz_gpu, mask_sz_gpu, mask_gpu, pts_gpu, cnts_gpu, E);
% Add a 'gather' function will take more than 0.3s: 
% E_freespace_gpu = double(gather(E_freespace_gpu)) / 20000;
fprintf('GPU: %2.6f\n', toc);
tic; [E_freespace_mex] = double(compute_score_mex(sz, mask_sz, mask, pts, cnts)) / 20000;
fprintf('MEX: %2.6f\n', toc);
tic; [E_freespace] = double(compute_score(sz, mask_sz, double(mask), pts, double(cnts))) / 20000;
fprintf('MATLAB: %2.6f\n', toc);
E_freespace_gpu = double(gather(E_freespace_gpu)) / 20000;
figure;imagesc([E_freespace_gpu, E_freespace_mex, E_freespace]);

